#include <string>
#include <arpa/inet.h>

struct Connection {
	Connection(unsigned long, unsigned short) {}
	const bool IsOk() const { return true; }
};
struct ConfigurationManager
{
	struct Instance
	{
		std::string GetValue(const std::string& configuration)
		{
			if (configuration.compare("address") == 0)
				return "192.168.1.2";
			else if (configuration.compare("port") == 0)
				return "8080";
			else
				return "";
		}
	};
};
struct Log
{
	struct Instance
	{
		void Write(const char*)
		{
		}
	};
};
struct ConnectionException : public std::runtime_error {
	ConnectionException(const char* arg) : std::runtime_error(arg) {}
};
///////////////////////////////////////////////////////////////////////////////////////////////////

std::string ConfigurationValueMissingError(const std::string& configuration)
{
	return std::string("Configuration value missing: ") + configuration;
}

std::string InvalidAddressError(const std::string& cfgAddress)
{
	return std::string("Invalid address: ") + cfgAddress;
}

std::string FailedToConnectError(const std::string& cfgAddress, const std::string& cfgPort)
{
	return std::string("Failed to connect: ") + cfgAddress + ":" + cfgPort;
}

void LogAndThrowConnectionException(const std::string& msg)
{
	Log::Instance().Write(msg.data());
	throw ConnectionException(msg.data());
}

Connection * CreateServerConnection()
{
	// Get address and check that it's OK (throw an exception if it's not)
	const std::string cfgAddress = ConfigurationManager::Instance().GetValue("address");
	if (cfgAddress.empty())
	{
		LogAndThrowConnectionException(ConfigurationValueMissingError("address"));
	}

	// Convert address to bytes and check that it's OK (throw an exception if it's not)
	in_addr addr;
	const int ret_inet_aton = inet_aton(cfgAddress.data(), &addr);
	if (ret_inet_aton==0)
	{
		LogAndThrowConnectionException(InvalidAddressError(cfgAddress));
	}
	const in_addr_t address = addr.s_addr;

	// Get port and check that it's OK (throw an exception if it's not)
	const std::string cfgPort = ConfigurationManager::Instance().GetValue("port");
	if (cfgPort.empty())
	{
		LogAndThrowConnectionException(ConfigurationValueMissingError("port"));
	}

	// Convert port to bytes
	const unsigned short port = htons(atoi(cfgPort.data()));

	// Create connection and check that it's OK (throw an exception if it's not)
	Connection* result = new Connection(address, port);
	if (!result || !result->IsOk())
	{
		LogAndThrowConnectionException(FailedToConnectError(cfgAddress, cfgPort));
	}

	// Return the connection
	return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

int main()
{
	Connection * c = CreateServerConnection();
	delete c;
	return 0;
}
